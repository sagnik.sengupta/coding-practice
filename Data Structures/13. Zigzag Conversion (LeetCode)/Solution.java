class Solution {
    public String convert(String s, int numRows) {
        if(numRows == 1)
            return s;
        String output = "";
        String arr[] = new String[numRows];
        boolean down = false;
        int row = 0;
        for(int i = 0; i < numRows; i ++)
            arr[i] = "";
        for(int i = 0; i < s.length(); i++) {
            if(row <= numRows) 
                arr[row] += s.charAt(i);
            if(row == numRows-1)
                down = false;
            if(row == 0)
                down = true;
            if(!down) 
                row--;
            else
                row++;
        }
        for(int i = 0; i < numRows; i++)
            output += arr[i];
        return output;
    }
}