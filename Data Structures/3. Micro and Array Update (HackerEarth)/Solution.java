import java.util.*;
class Solution
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        while(T>0)
        {
            int N = sc.nextInt();
            int K = sc.nextInt();
            int min=Integer.MAX_VALUE;
            for(int i=0;i<N;i++)
            {
                int a=sc.nextInt();
                if(a<min)
                min=a;
            }
            if((K-min)<=0)
            System.out.println(0);
            else
            System.out.println(K-min);
            T--;
        }
    }
}