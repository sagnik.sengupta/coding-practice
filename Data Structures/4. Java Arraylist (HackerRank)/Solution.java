import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int n = sc.nextInt();
        ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < n; i++) {
            int d = sc.nextInt();
            ArrayList<Integer> elements = new ArrayList<Integer>();
            for (int j = 0; j < d; j++) {
                elements.add(sc.nextInt());
            }
            list.add(i, elements);
        }
        int q = sc.nextInt();
        for (int i = 1; i <= q ; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();
            if(list.get(x-1).size()>0 && list.get(x-1).size() >= y)
                System.out.println(list.get(x-1).get(y-1));
            else
                System.out.println("ERROR!");
        }
        sc.close();   
    }
}
