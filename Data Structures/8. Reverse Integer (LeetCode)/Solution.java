class Solution {
    public int reverse(int x) {
        int rev_num = 0;
        for(int i = x; i  != 0 ; i/=10) {
            
            if (rev_num > Integer.MAX_VALUE/10 || (rev_num == Integer.MAX_VALUE / 10 && i%10 > 7)) 
                return 0;
            if (rev_num < Integer.MIN_VALUE/10 || (rev_num == Integer.MIN_VALUE / 10 && i%10 < -8)) 
                return 0;
        
            rev_num = rev_num * 10 + i%10;
        }
        return rev_num;
    }
}