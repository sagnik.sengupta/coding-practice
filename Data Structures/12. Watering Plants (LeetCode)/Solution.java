class Solution {
    public int wateringPlants(int[] plants, int capacity) {
        int steps = 0;
        int cap = capacity;
        for (int i = 0; i < plants.length; i ++) {
            if (capacity >= plants[i]) {
                steps++;
                capacity -= plants[i];
            }
            else {
                capacity = cap;
                steps += 2*i+1;
                capacity -= plants[i];
            }
        }
        return steps;
    }
}