class Solution {
    public int romanToInt(String s) {
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<Character, Integer>();
        ArrayList<Integer> list = new ArrayList<Integer>();
        int sum = 0;
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        for (int i = 0; i < s.length(); i++) {
            list.add(map.get(s.charAt(i)));
        }
        for (int i = 0 ; i < s.length(); i++) {
            if (i !=s.length()-1 && list.get(i) < list.get(i+1)) {
                sum+= list.get(i+1) - list.get(i);
                i++;
            }
            else 
                sum +=list.get(i);
        } 
        return sum;
    }
}