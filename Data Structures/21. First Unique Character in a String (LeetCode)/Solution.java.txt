class CountIndex {
    int count, index;
    public CountIndex(int index) {
        this.count  = 1;
        this.index = index;
    }
    public void incCount()
    {
        this.count++;
    }
}

class Solution {
    public int firstUniqChar(String s) {
        int no_of_chars = 256;
        int result = Integer.MAX_VALUE;
        HashMap<Character, CountIndex> hm = new HashMap<Character, CountIndex>(no_of_chars);
        for(int i = 0; i < s.length(); i++) {
            if(hm.containsKey(s.charAt(i))) {
                hm.get(s.charAt(i)).incCount();
            }
            else 
                hm.put(s.charAt(i), new CountIndex(i));
        }
        for (Map.Entry<Character, CountIndex> entry : hm.entrySet()) {
            int c = entry.getValue().count;
            int ind = entry.getValue().index;
            if (c == 1 && ind < result) {
                result = ind;
            }
        }
        if (result != Integer.MAX_VALUE)
            return result;
        else
            return -1;
    }
}