/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */

class Solution {
    public boolean isPalindrome(ListNode head) {
        ListNode slowPointer = head;
        boolean isPalindrome = true;
        Stack<Integer> stack = new Stack<Integer>();
        while (slowPointer != null) {
            stack.push(slowPointer.val);
            slowPointer = slowPointer.next;
        }
        
        while (head != null) {
            if (head.val == stack.pop()) {
                isPalindrome = true;
            }
            else {
                isPalindrome = false;
                break;
            }
            head = head.next;
        }
        return isPalindrome;
    }
}