class Solution {
    public int countConsistentStrings(String allowed, String[] words) {
        int input[] = new int[26];
        int count = 0;
        for(int i = 0; i < allowed.length(); i++)
            input[allowed.charAt(i)-'a'] = 1;
        for (String s: words) {
            boolean isConsistent = true;
            for (int i = 0; i < s.length(); i++){
                if(input[s.charAt(i)-'a'] == 0)
                {
                    isConsistent = false;
                    break;
                }   
            }
            if (isConsistent)
                    count++;
        }
        return count;
    }
}