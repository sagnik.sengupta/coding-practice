class Solution {
    public int singleNumber(int[] nums) {
        return Arrays.stream(nums).reduce(0, (x,y) -> x^y);
    }
}