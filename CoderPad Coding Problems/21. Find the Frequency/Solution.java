import java.io.*;
import java.util.*;

class FastReader{ 
    BufferedReader br; 
    StringTokenizer st; 

    public FastReader(){ 
        br = new BufferedReader(new InputStreamReader(System.in)); 
    } 

    String next(){ 
        while (st == null || !st.hasMoreElements()){ 
            try{ st = new StringTokenizer(br.readLine()); } catch (IOException  e){ e.printStackTrace(); } 
        } 
        return st.nextToken(); 
    } 

    String nextLine(){ 
        String str = ""; 
        try{ str = br.readLine(); } catch (IOException e) { e.printStackTrace(); } 
        return str; 
    } 
    
    Integer nextInt(){
        return Integer.parseInt(next());
    }
} 
    
class Solution {
    public static void main(String args[]) throws IOException {
        FastReader sc = new FastReader();
        PrintWriter out = new PrintWriter(System.out);
        int t = sc.nextInt();
        while (t-- > 0) {
            Frequency ob = new Frequency();
            int N = sc.nextInt(), A[] = new int[N];
    	    for(int i = 0;i<N;i++){
    	        A[i] = sc.nextInt();
    	    }
    	    int x = sc.nextInt();
    	    out.println(ob.findFrequency(A, x));
        }
        out.flush();
    }
}

class Frequency{
    int findFrequency(int A[], int x){
        return (int)Arrays.stream(A).filter(i-> i==x).count();
    }
}