class Solution {
    public int[] countBits(int n) {
        int output[] = new int[n+1];
        for(int i = 0; i <= n ; i++)
            output[i] = noOfOnes(i);
        return output;
    }
    public int noOfOnes(int num) {
        int count = 0;
        while(num > 0){
            num &= (num-1);
            count++;
        }
        return count;
    }
}