class Solution {
    public int missingNumber(int[] nums) {
        int array_sum =  Arrays.stream(nums).reduce(0, (x,y) -> x+y);
        int total_sum = nums.length*(nums.length+1)/2;
        return total_sum - array_sum;
    }
}