class Solution {
    public int[] smallerNumbersThanCurrent(int[] nums) {
        int sortedArray[] = nums.clone();
        Arrays.sort(sortedArray);
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < sortedArray.length; i++){
            if(map.get(sortedArray[i]) == null)
                map.put(sortedArray[i], i);
        }
        for(int i = 0; i < nums.length; i++)
            nums[i] = map.get(nums[i]);
        return nums;
    }
}