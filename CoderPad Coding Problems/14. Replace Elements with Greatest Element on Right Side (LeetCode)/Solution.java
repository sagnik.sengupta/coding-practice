class Solution {
    public int[] replaceElements(int[] arr) {
        int max = -1;
        int output[] = new int[arr.length];
        output[arr.length-1] = max;
        for(int i = arr.length-1; i > 0; i--) {
            if(arr[i] > max) {
                max = arr[i];
            }
            output[i-1] = max;
        }
        return output;
    }
}