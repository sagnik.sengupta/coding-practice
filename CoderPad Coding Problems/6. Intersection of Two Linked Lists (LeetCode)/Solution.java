/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        int count1 = countNode(headA);
        int count2 = countNode(headB);
        if (count1 > count2)
            return findIntersection(count1-count2, headA, headB);
        else 
            return findIntersection(count2-count1, headB, headA);
    }
    public ListNode findIntersection(int d, ListNode currentA, ListNode currentB) {
        while(d-->0) {
            if(currentA == null) {
                return null;
            }
            currentA = currentA.next;
        }
        while(currentA!=null && currentB!=null) {
            if(currentA == currentB) {
                return currentA;
            }
            currentA = currentA.next;
            currentB = currentB.next;
        }
        return null;
    }
    public int countNode(ListNode head) {
        int count = 0;
        while(head != null) {
            count++;
            head = head.next;
        }
        return count;
    }
}