class Solution {
    public int countPrimes(int n) {
        boolean prime[] = new boolean[n];
        Arrays.fill(prime, true);
        for (int p = 2; p * p < n; p++) {
            if (prime[p] == true) {
                for (int i = p * p; i < n; i += p)
                    prime[i]= false;
            }
        }
        int count=0;
        for(int i=2;i<n;i++){
            if(prime[i]==true){
                count++;
            }
        }
        return count;
    }
}


//Solution using Java 8, which is giving TLE in LeetCode
/*class Solution {
    public int countPrimes(int n) {
        if (n == 0 || n == 1) 
            return 0;
        List<Boolean> prime = new ArrayList<>(n);       
        for(int i = 0; i < n; i++)
            prime.add(true);
        for (int p = 2; p * p < n; p++) {
            if (prime.get(p) == true) {
                for (int i = p * p; i < n; i += p)
                    prime.set(i,false);
            }
        }
        return (int)prime.stream().filter(x -> x == true).count()-2;
	}
}*/