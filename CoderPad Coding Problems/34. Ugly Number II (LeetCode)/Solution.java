class Solution {
    public int nthUglyNumber(int n) {
        int dp[] = new int[n];
        dp[0] = 1;
        int count_2 = 0, count_3 = 0, count_5 = 0;
        for(int i = 1; i < n; i++) {
            dp[i] = Math.min(Math.min(2*dp[count_2], 3*dp[count_3]), 5*dp[count_5]);
            if (dp[i] == 2*dp[count_2])
                count_2++;
            if (dp[i] == 3*dp[count_3])
                count_3++;
            if (dp[i] == 5*dp[count_5])
                count_5++;
        }
        return dp[n-1];
    }
}