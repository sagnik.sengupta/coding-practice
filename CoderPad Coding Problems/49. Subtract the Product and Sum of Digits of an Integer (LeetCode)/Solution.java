class Solution {
    public int subtractProductAndSum(int n) {
        int product = 1, sum = 0;
        for(int i = n; i > 0; product *= i%10, sum += i%10, i/=10);
        return product - sum;
        
    }
}