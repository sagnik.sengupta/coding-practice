class Solution {
    public String sortSentence(String s) {
        int space = 0;
        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i) == ' ')
                space++;
        String words[] = new String[space+1];
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if(!Character.isDigit(s.charAt(i))) {
                if(s.charAt(i) != ' ')
                    str.append(s.charAt(i));
            }
            else {
                words[s.charAt(i) - '0' - 1] = str.toString();
                str.setLength(0);
            }
            
        }
        for (int i = 0; i < words.length; i++)
            str.append(words[i]).append(' ');
        return str.deleteCharAt(str.length()-1).toString();
    }
}