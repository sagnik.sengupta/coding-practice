/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) { 
        List<Integer> output = new ArrayList<>();
        inorderTree(output, root);
        return output;
    }
    public void inorderTree(List<Integer> output, TreeNode root) {
        if(root == null)
            return;
        else {
            inorderTree(output, root.left);
            output.add(root.val);
            inorderTree(output, root.right);
        }
        
    } 
}