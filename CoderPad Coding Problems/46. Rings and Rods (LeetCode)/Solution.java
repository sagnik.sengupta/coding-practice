class Solution {
    public int countPoints(String rings) {
        Set<Character> red = new HashSet<>();
        Set<Character> green = new HashSet<>();
        Set<Character> blue = new HashSet<>();
        int count = 0;
        for(int i = 1; i < rings.length(); i+=2){
            if(rings.charAt(i-1) == 'R')
                red.add(rings.charAt(i));
            else if(rings.charAt(i-1) == 'G')
                green.add(rings.charAt(i));
            else if(rings.charAt(i-1) == 'B')
                blue.add(rings.charAt(i));
        }
        for(char i = 48; i <= 57; i++)
            if(red.contains(i) && green.contains(i) && blue.contains(i))
                count++;
        return count;
    }
}