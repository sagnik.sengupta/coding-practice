class Solution {
    public int[] pivotArray(int[] nums, int pivot) {
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] < pivot)
                left.add(nums[i]);
            else if(nums[i] > pivot)
                right.add(nums[i]);
        }
        for(int i = 0; i < left.size(); i++)
            nums[i] = left.get(i);
        for(int i = 0; i < nums.length - (left.size() + right.size()); i++)
            nums[left.size() + i] = pivot;
        for(int i = 0; i < right.size(); i++)
            nums[left.size() + nums.length - (left.size() + right.size()) + i] = right.get(i);
        return nums;
    }
}