class Solution {
    public int numJewelsInStones(String jewels, String stones) {
        HashMap<Character, Integer> map = new HashMap<>();
        int sum = 0;
        for(int i = 0; i < stones.length(); i++)
        {
            if(map.get(stones.charAt(i)) == null)
                map.put(stones.charAt(i), 1);
            else
                map.put(stones.charAt(i), map.get(stones.charAt(i))+1);
        }
        for(int i = 0; i < jewels.length(); i++){
            sum += map.get(jewels.charAt(i)) == null ? 0 : map.get(jewels.charAt(i));
        }
        return sum;
    }
}