class Solution {
    public int maxProfit(int[] prices) {
        int profit = Integer.MIN_VALUE;
        int max_profit = Integer.MIN_VALUE;
        int minSoFar = prices[0];
        for (int i = 0; i < prices.length; i++) {
            minSoFar = Math.min(minSoFar, prices[i]);
            profit = prices[i] - minSoFar;
            max_profit = Math.max(max_profit, profit);
        }
        return max_profit;
    }
}