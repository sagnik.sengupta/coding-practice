class Solution {
    public int[] plusOne(int[] digits) {
        int n = digits.length;
        digits[n-1] += 1;
        for (int i = n-1; i > 0; i--) {
            if(digits[i] == 10) {
                digits[i] = 0;
                digits[i-1] += 1;
            }
        }
        if(digits[0] == 10){
            int[] new_digits = new int[n+1];
            new_digits[0] = 1;
            new_digits[1] = 0;
            for(int i = 2; i <= n; i++)
                new_digits[i] = digits[i-1];
            return new_digits;
        }
        return digits;
    }
}