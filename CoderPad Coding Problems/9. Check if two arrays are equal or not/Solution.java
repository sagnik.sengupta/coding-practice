import java.util.*;
class Solution {
    public static void main(String args[]) {
        int arr1[] = { 3, 5, 2, 5, 2 };
        int arr2[] = { 2, 3, 5, 5, 2 };
        System.out.println(areEqual(arr1, arr2)==true?"Equal":"Not Equal");
    }
    public static boolean areEqual(int arr1[], int arr2[]) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < arr1.length; i++) {
            if(map.get(arr1[i]) == null) 
                map.put(arr1[i], 1);
            else {
                int count = map.get(arr1[i]);
                count++;
                map.put(arr1[i], count);
            }
        }
        
        for(int i = 0; i < arr2.length; i++) {
            if(!map.containsKey(arr2[i]))
                return false;
            if(map.get(arr2[i]) == 0)
                return false;
            
            int count = map.get(arr2[i]);
            count--;
            map.put(arr2[i],count);
        }
        return true;
    }
}