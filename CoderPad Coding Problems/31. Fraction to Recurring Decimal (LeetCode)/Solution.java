class Solution {
    public String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) 
            return "0";
        StringBuilder str = new StringBuilder();
        if (numerator < 0 && denominator > 0 || numerator > 0 && denominator < 0)
            str.append("-");
        
        long dividend = Math.abs((long)numerator);
        long divisor = Math.abs((long)denominator);
        long remainder = dividend % divisor;
        str.append(dividend / divisor);
        
        if (remainder == 0)
            return str.toString();
        
        str.append(".");
        HashMap<Long, Integer> map = new HashMap<>();
        while (remainder != 0) {
            if(map.containsKey(remainder)) {
                str.insert(map.get(remainder), "(");
                str.append(")");
                break;
            }
            map.put(remainder, str.length());
            remainder *= 10;
            str.append(remainder / divisor);
            remainder %= divisor;
        }
        return str.toString();
    }
    
}