import java.util.*;
class Solution {
    public static void main (String args[]) {
        String str = "wwwwaaadexxxxxxywww";
        encoding(str);
    }
    public static void encoding (String str) {
        StringBuilder output = new StringBuilder();
        Pattern pattern = Pattern.compile("(([a-zA-Z])\\2*)");
        pattern.matcher(str).results().forEach(result -> output.append(result.group(2)).append(result.group(1).length()));
        System.out.println(output);
    }
}