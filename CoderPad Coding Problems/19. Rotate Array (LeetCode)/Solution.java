class Solution {
    public void rotate(int[] nums, int k) {
        if (nums.length == 1 || nums.length == k)
            return;
        k %= nums.length;
        int pivot = nums.length - k;
        reverse(nums, 0, pivot-1);
        reverse(nums, pivot, nums.length-1);
        reverse(nums, 0, nums.length-1);
    }
    public void reverse(int[] nums, int low, int high) {
        while (low < high) {
            int temp = nums[low];
            nums[low] = nums[high];
            nums[high] = temp;
            low++;
            high--;
        }
    }
}