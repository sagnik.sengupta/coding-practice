class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<Integer> current = new ArrayList<>();
        List<List<Integer>> ans = new ArrayList<>();
        addSubsetsToList(nums, 0, current, ans);
        return ans;
    }
    public void addSubsetsToList(int[] nums, int i, List<Integer> current, List<List<Integer>> ans) {
        if (i == nums.length)
        {
            ans.add(new ArrayList<Integer>(current));
            return;
        }
        current.add(nums[i]);
        addSubsetsToList(nums, i+1, current, ans);
        current.remove(current.size()-1);
        addSubsetsToList(nums, i+1, current, ans);
    }
}