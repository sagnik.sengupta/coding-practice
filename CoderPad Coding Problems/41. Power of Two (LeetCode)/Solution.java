class Solution {
    public boolean isPowerOfTwo(int n) {
        if (n < 0) 
            return false;
        String s = Integer.toBinaryString(n);
        int count = 0;
        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i) == '1')
                count++;
        
        return count == 1;
    }
}